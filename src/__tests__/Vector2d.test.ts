import { Vector2d } from '../index';

test('Multiplication', () => {
  const v = new Vector2d(1, 2);
  const q = v.mul(-10);
  expect(v.x).toBe(1);
  expect(v.y).toBe(2);
  expect(q.x).toBe(-10);
  expect(q.y).toBe(-20);
});

test('Addition', () => {
  const v = new Vector2d(1, 2);
  const q = new Vector2d(-1, -2);
  expect(v.add(q).length).toBe(0);
  expect(v.eq(new Vector2d(1, 2))).toBe(true);
  expect(v.sub(v).length).toBe(0);
});

test('Create vector from list', () => {
  const v = Vector2d.fromList([1, 2]);
  const q = new Vector2d(1, 2);
  expect(v.eq(q)).toBe(true);
});

test('Create vector from object', () => {
  const v = Vector2d.fromObject({ x: 1, y: 2 });
  const q = new Vector2d(1, 2);
  expect(v.eq(q)).toBe(true);
});

test('Get vector length', () => {
  const v = new Vector2d(1, 1);
  const q = new Vector2d(1, 0);
  expect(v.length).toBeCloseTo(Math.SQRT2, 10);
  expect(q.length).toBe(1);
});

test('Set vector length and normalize', () => {
  const v = new Vector2d(1, 1);
  v.length = 1;
  expect(v.length).toBe(1);
  expect(v.x === v.y).toBe(true);
  const q = new Vector2d(1, 2);
  const r = q.normalized();
  expect(q.eq(new Vector2d(1, 2))).toBe(true);
  expect(r.length).toBe(1);
});

test('Set length must throw error', () => {
  const v = new Vector2d();
  expect(() => {
    v.length = 1;
  }).toThrowError(Vector2d.errors.setWhenLengthIsZero);
});
