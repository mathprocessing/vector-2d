export class Vector2d {
  static errors = {
    setWhenLengthIsZero: "Length property can't be changed for Vector2d(0, 0)",
  };

  constructor(public x: number = 0, public y: number = 0) {}

  /**
   * Create new vector from list `[x, y]`.
   * @param param0
   */
  static fromList([x, y]: [number, number]) {
    return new Vector2d(x, y);
  }

  /**
   * Create new vector from object `{x, y}`.
   * @param vectorObject
   */
  static fromObject(vectorObject: { x: number; y: number }) {
    const { x, y } = vectorObject;
    return new Vector2d(x, y);
  }

  /**
   * @returns copy of instance
   */
  copy() {
    return new Vector2d(this.x, this.y);
  }

  /**
   * @param other
   * @returns `true` if `this` vector to be equal to `other` vector
   */
  eq(other: Vector2d) {
    return this.x === other.x && this.y === other.y;
  }

  /**
   * Add `other` vector to `this` vector
   * @param other
   */
  add(other: Vector2d) {
    return new Vector2d(this.x + other.x, this.y + other.y);
  }

  /**
   * Subtract `other` vector from `this` vector.
   * @param other
   */
  sub(other: Vector2d) {
    return new Vector2d(this.x - other.x, this.y - other.y);
  }

  /**
   * Multiplicate vector by constant `lambda`.
   * @param lambda constant number
   */
  mul(lambda: number) {
    return new Vector2d(this.x * lambda, this.y * lambda);
  }

  /**
   * Calculate scalar product
   * @param other
   * @returns Scalar product of `this` and `other` vectors
   */
  dot(other: Vector2d) {
    return this.x * other.x + this.y * other.y;
  }

  /**
   * @return length of the vector.
   */
  get length() {
    return Math.hypot(this.x, this.y);
  }

  /**
   * Change length of the vector, but direction stay the same.
   *
   * Can be used as normalize:
   * ```js
   * v.length = 1; // normalize vector
   * ```
   */
  set length(targetLength: number) {
    const len = this.length;
    if (len === targetLength) {
      return;
    }
    if (len === 0) {
      throw new Error(Vector2d.errors.setWhenLengthIsZero);
    }
    const modified = this.mul(targetLength / len);
    this.x = modified.x;
    this.y = modified.y;
  }

  /**
   * @return new normalized vector
   */
  normalized() {
    const modified = this.copy();
    modified.length = 1;
    return modified;
  }
}
