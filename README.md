# About
This is a example typescript npm package "vector-2d"

# How to Install vector-2d npm package from git repo
```
npm i git+https://codeberg.org/mathprocessing/vector-2d.git
```
or
```
pnpm i git+https://codeberg.org/mathprocessing/vector-2d.git
```
or
```
yarn add https://codeberg.org/mathprocessing/vector-2d
```

### About yarn
https://stackoverflow.com/questions/43411864/how-to-install-package-from-github-repo-in-yarn
```
yarn add https://github.com/fancyapps/fancybox [remote url]
yarn add ssh://github.com/fancyapps/fancybox#3.0  [branch]
yarn add https://github.com/fancyapps/fancybox#5cda5b529ce3fb6c167a55d42ee5a316e921d95f [commit]
```

# How to use package
### 1. Create or change this files in root dir
* `tsconfig.json`
```json
{
  "compilerOptions": {
    "target": "ES2015",
    "module": "Commonjs",
    "strict": true,
    "strictNullChecks": true
  },
  "include": ["src"],
  "exclude": ["node_modules"]
}
```

* `package.json`
```json
{
  "name": "test-pnpm-install",
  "version": "1.0.0",
  "description": "",
  "private": true,
  "main": "main.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1",
    "execute": "ts-node src/main.ts"
  },
  "author": "",
  "license": "ISC",
  "dependencies": {
    "vector-2d": "git+https://codeberg.org/mathprocessing/vector-2d.git"
  }
}
```

* `src/main.ts`
```ts
import { Vector2d } from 'vector-2d';

const v = new Vector2d(3, 4);
v.length = 10;
console.assert(Math.sqrt(3 ** 2 + 4 ** 2) === 5);
console.log(v); // Vector2d { x: 6, y: 8 }

// Check that result is correct
const result = Vector2d.fromObject({ x: 6, y: 8 });
console.assert(v.eq(result));
```

### 2. Install devDependencies

* Install `ts-node` globally or locally
```bash
# Globally
npm i -g ts-node typescript
# Locally
npm i --save-dev ts-node typescript @types/node
```

### 3. Run ts-node from npm scripts
```
npm run execute
```

# Help info
### `.npmignore` 
```text
src
tsconfig.json
tslint.json
.prettierrc
```

Note: We don't need to use `.npmignore` because  we already added `files` property in `package.json`.

### Install from local dir
https://docs.npmjs.com/cli/v8/commands/npm-install
```
NOTE: If you want to install the content of a directory like a package from the registry instead of creating a link, you would need to use npm pack while in the <folder> directory, and then install the resulting tarball instead of the <folder> using npm install <tarball file>
```

Create link from tarball (file should be .tar, tar.gz or .tgz)
```
npm install ./package-name.tar.gz
```

Create link from folder
```
npm install ./package-dir
```

How to use `npm pack`:
* Just run `npm pack` in directory that contains `package.json`

For example:
```
cd package-dir
npm pack
cp package-name-0.1.0.tgz ~
cd example-project-dir
npm install ~/package-name-0.1.0.tgz
```

#### CL Options of npm pack

TODO: How to use --pack-destination option?

### Install from remote repo
* `npm install githubname/reponame`
* `npm install git+https://github.com/visionmedia/express.git`
  or
  `npm install git+https://github.com/visionmedia/express.git#v1.0.27`
  see: https://atmos.washington.edu/~nbren12/reports/journal/2018-07-16-NN-conservation/node_modules/npm/html/doc/cli/npm-install.html
  
* link to repo: `https://github.com/Amitesh/gulp-rev-all`
  install from tarball: `npm install --save https://github.com/Amitesh/gulp-rev-all/tarball/master`

* npm install `<tarball url>`

Fetch the tarball url, and then install it. In order to distinguish between this and other options, the argument must start with "http://" or "https://"

Example:
    
    npm install https://github.com/indexzero/forever/tarball/v0.5.6


-----

Bad:
* npm install https://codeberg.org/mathprocessing/vector-2d/archive/v1.0.1.tar.gz
Install only all src files

Note: This is bad because package don't packed.

-----

Good:
1. Local install
```
npm pack
npm install package-tarball.tgz
```
or
2. Install from git repo
```
npm install git+https://github.com/visionmedia/express.git
```

### Change version
https://docs.npmjs.com/cli/v7/commands/npm-version
```
npm version patch -m "Upgrade to %s for reasons"
```
Note: `%s` replaced by resulting version number

# Troubleshooting
* If vscode don't see `index.d.ts` in `node_modules` try to reload it.
  + `Ctrl + Shift + P`
  + `>Developer: Reload Window`

# Additional links
* [MAKING IT EASIER TO WORK WITH LOCAL NPM PACKAGES](https://www.aaron-powell.com/posts/2020-03-02-making-it-easier-to-work-with-local-npm-packages/)
* [Step by step: Building and publishing an NPM Typescript package.](https://itnext.io/step-by-step-building-and-publishing-an-npm-typescript-package-44fe7164964c)
